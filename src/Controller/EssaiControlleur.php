<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("/essai")
 */
class EssaiControlleur extends AbstractController
{

    /**
     * @Route("/home", name="essai_home_view")
     */
    public function index()
    {
        $response = $this->render('layout.html.twig');

        return $response;
    }
    /**
     * @Route("/add", name="essai_add_view")
     */
    public function add()
    {
        return $this->render('essai/add.html.twig');
    }
    /**
     * @Route("/edit", name="essai_edit_view")
     */
    public function edit()
    {
        return $this->render('essai/edit.html.twig');
    }

    /**
     * @Route("/affiche", name="essai_affiche_view")
     */
    public function affiche()
    {
        $listAdverts = array(
            array(
              'title'   => 'Recherche développpeur Symfony',
              'id'      => 1,
              'author'  => 'Alexandre',
              'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
              'date'    => new \Datetime()),
            array(
              'title'   => 'Mission de webmaster',
              'id'      => 2,
              'author'  => 'Hugo',
              'content' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
              'date'    => new \Datetime()),
            array(
              'title'   => 'Offre de stage webdesigner',
              'id'      => 3,
              'author'  => 'Mathieu',
              'content' => 'Nous proposons un poste pour webdesigner. Blabla…',
              'date'    => new \Datetime())
          );

        return $this->render('essai/annonces.html.twig',array(
            'listAdverts' => $listAdverts
          ));
    }
}
