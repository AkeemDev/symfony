<?php
// src/Controller/AdvertController.php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class AdvertController2
{
  public function index(Environment $twig)
  {
    $content = $twig->render('Advert/index.html.twig', ["name" => "Au revoir Mon nom","variable1" => "variable numéro 10","variable2" => "variable numéro 20","variable3" => "variable numéro 30"]);
    $response = new Response($content);

    return $response;
  }
}