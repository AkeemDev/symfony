<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CedricController extends AbstractController
{
    /**
     * @Route("/cedric", name="cedric")
     */
    public function index()
    {
        return $this->render('cedric/index.html.twig', [
            'controller_name' => 'CedricController',
        ]);
    }
}
