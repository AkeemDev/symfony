<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/service")
 */
class MonService extends AbstractController
{
    /**
     * @Route("/mail", name="serviceMail")
     */
    public function seriveMail()
    {

        // On a donc accès au conteneur :
        $mailer = $this->container->get('mailer');

        // On peut envoyer des e-mails, etc.
    }
}
